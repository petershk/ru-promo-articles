# Удобство и продуктивность, ч. 59

Это 59-я неделя инициативы «Удобство и продуктивность», и у нас припасено много вкусных улучшений для наших приложений! В дополнение к большому числу правок в Plasma и Frameworks, мы усиленно работали над новым функционалом KDE Applications 19.04, до релиза которых осталось чуть меньше двух месяцев. Просто взгляните:

## Новые возможности

[Реализована поддержка KRun в порталах XDG](https://bugs.kde.org/show_bug.cgi?id=399380), что позволит изолированным приложениям Flatpak открывать содержимое в других программах, используя типичное для KDE окно выбора приложения (Денис Карповский, KDE Frameworks 5.56)

У утилиты для создания снимка экрана Spectacle [появилась новая опция по умолчанию](https://bugs.kde.org/show_bug.cgi?id=391299) для [запоминания текущей прямоугольной области до закрытия утилиты](https://phabricator.kde.org/D19117) (David Redondo, KDE Applications 19.04.0)

Spectacle [теперь позволяет пользователям настраивать, что произойдёт при нажатии горячей клавиши снимка экрана, если Spectacle уже запущен](https://phabricator.kde.org/T9855) (David Redondo, KDE Applications 19.04.0):

![0](https://i.imgur.com/U1H6fo8.png)

Просмотрщик изображений Gwenview [теперь умеет открывать файлы изображений Krita](https://bugs.kde.org/show_bug.cgi?id=337272) (Nate Graham, KDE Applications 19.04.0)

В Gwenview [теперь можно переключаться на следующее и предыдущее изображение с помощью кнопок «Вперёд» и «Назад», которые присутствуют на многих мышках](https://phabricator.kde.org/D14583) (Anthony Fieroni, KDE Applications 19.04.0)

В эмуляторе терминала Konsole [теперь можно создавать новые вкладки щелчком средней кнопки мыши по пустому месту в панели вкладок и закрывать их щелчком средней кнопки мыши по ним](https://bugs.kde.org/show_bug.cgi?id=398940) (Shubham, KDE Applications 19.04.0)

## Исправления ошибок и улучшения производительности

Исправлен случай, при котором окно [могло появиться дважды в обзоре всех рабочих столов](https://bugs.kde.org/show_bug.cgi?id=404442) (Влад Загородний, KDE Plasma 5.15.1)

Настройка средней кнопки мыши на открытие меню запуска приложений [больше не ломает несвязанные с этим части рабочего стола Plasma](https://bugs.kde.org/show_bug.cgi?id=404532) (Kai Uwe Broulik, KDE Plasma 5.15.1)

Центр приложений Discover [больше не падает, если был открыт с неправильным URL-адресом `appstream://`](https://bugs.kde.org/show_bug.cgi?id=404402) (Aleix Pol Gonzalez, KDE Plasma 5.15.2)

Меню запуска приложений по умолчанию [больше не реагирует, неожиданно для пользователя и неуместно, на комбинации клавиш для изменения расположения и распахивания окна](https://bugs.kde.org/show_bug.cgi?id=402614) (Nate Graham, KDE Plasma 5.15.2)

Меню запуска приложений по умолчанию [больше не позволяет имени пользователя и информации о системе перекрывать друг друга в некоторых ситуациях](https://bugs.kde.org/show_bug.cgi?id=404338) (Matthieu Gras, KDE Plasma 5.15.2)

Тени меню и окон Breeze по умолчанию [были возвращены к чистым чёрным тонам, что улучшает видимость тени при использовании цветовой схемы «Breeze, тёмный вариант»](https://bugs.kde.org/show_bug.cgi?id=400253) (Noah Davis, KDE Plasma 5.16.0):

![1](https://i.imgur.com/VpWabQP.png)

Опция «Поделиться через Imgur», доступная во многих приложениях KDE, [теперь действительно работает](https://bugs.kde.org/show_bug.cgi?id=397567) (Aleix Pol Gonzalez, KDE Frameworks 5.56)

Диспетчер файлов Dolphin и другие приложения KDE [теперь показывают больше метаданных для приложений AppImage, если в системе установлена свежая версия библиотеки `libappimage`](https://phabricator.kde.org/D18450) (Friedrich Kossebau, KDE Frameworks 5.56):

![2](https://phabricator.kde.org/file/data/bvnhvp2wctl5e5snlgmp/PHID-FILE-yqyu3wsgwllsh3ccwgjd/Screenshot_20190122_102745.png)

Dolphin [больше не падает при закрытии, если одна из комнат Plasma была закрыта с запущенным Dolphin](https://bugs.kde.org/show_bug.cgi?id=402784) (David Hallas, KDE Applications 18.12.3)

[Исправлено странное отображение некоторых символов в Konsole](https://bugs.kde.org/show_bug.cgi?id=402415) (Mariusz Glebocki, KDE Applications 19.04.0)

## Улучшения пользовательского интерфейса

Discover больше не показывает [вводящее в заблуждение сообщение, которое просит проверить ваше соединение, во время загрузки содержимого окна сразу после запуска программы](https://bugs.kde.org/show_bug.cgi?id=404610) (Aleix Pol Gonzalez, KDE Plasma 5.15.2)

На странице обновления системы при помощи Discover длинные версии пакетов [больше не обрезаются, если горизонтального места оказывается недостаточно, например, при использовании широкоэкранного вида и просмотре страницы описания пакета](https://bugs.kde.org/show_bug.cgi?id=404624) (Nate Graham, KDE Plasma 5.15.2):

![3](https://phabricator.kde.org/file/data/rwwjwgllvugcml3smy6d/PHID-FILE-wsh5hhhl6dmtk2jrbet6/Squeezey.png)

Были внесены [значительные улучшения в пользовательский интерфейс страницы «Смайлики» Параметров системы](https://phabricator.kde.org/D19093) (Björn Feber, KDE Plasma 5.16.0):

![4](https://i.imgur.com/BDB6hAa.png)

На странице обновлений в Discover [было улучшено отображение числа обновлений в заголовке](https://bugs.kde.org/show_bug.cgi?id=404603) (Nate Graham, KDE Plasma 5.16.0):

![5](https://phabricator.kde.org/file/data/5tdzoaawr67chmd5gf2s/PHID-FILE-hd4ckx6owfqoahq4yoap/Some_updates_selected.png)

[Появились](https://phabricator.kde.org/D19020) [новые значки](https://phabricator.kde.org/D19074) для спящего режима, ждущего режима и переключения пользователей (Krešimir Čohar, KDE Frameworks 5.56):

![6](https://i.imgur.com/THApxPD.png)

Появился [новый крутой значок видеокамеры](https://phabricator.kde.org/D18986), который вскоре начнёт использоваться в [различных местах](https://phabricator.kde.org/D19234) (Rafael Brandmaier и Krešimir Čohar, KDE Frameworks 5.56):

![7](https://phabricator.kde.org/file/data/li4cgzqqde4qgsrxlqin/PHID-FILE-clpgoyf5oxpkhigxr3kx/montage-breeze-Background.png)

Если кнопка на панели инструментов в приложении на основе библиотеки Kirigami при нажатии открывает всплывающее меню, то она [больше не показывает свою всплывающую подсказку при открытом меню](https://bugs.kde.org/show_bug.cgi?id=404371) (Aleix Pol Gonzalez, KDE Frameworks 5.56)

На следующей неделе ваше имя может появиться в этом списке! Не знаете, с чего начать? Просто спросите! Я недавно смог помочь нескольким новым участникам, буду рад помочь и вам! Вы также можете прочитать статью по ссылке <https://kde.ru/join> и выяснить, как стать частью того, что действительно имеет значение. Вам не требуется быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Если вы находите программное обеспечение KDE полезным, рассмотрите возможность [пожертвования средств](https://kde.org/ru/community/donations/) в [фонд KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2019/02/24/this-week-in-usability-productivity-part-59/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
