# На этой неделе в KDE: дневная цветовая температура

> 28 августа – 4 сентября, основное — прим. переводчика

Мы приближаемся к бета-версии Plasma 5.26, и много новых функций было добавлено до их «мягкой» заморозки — момента, после которого целесообразность внедрения любых новшеств тщательно обсуждается. Многие упомянуты ниже! Теперь же план на ближайшие 6 недель до финального выпуска Plasma 5.26 заключается в том, чтобы сконцентрироваться на исправлении ошибок и доработке пользовательского интерфейса, и помощь чрезвычайно приветствуется. Если вы разработчик и вам нравится Plasma, **время исправлять ошибки**! Какие ошибки? [Вот эти](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2145075&order=priority%2Cchangeddate%20DESC%2Cbug_status%2Cassigned_to%2Cbug_id&query_format=advanced&version=5.24.90&version=5.25.0&version=5.25.1&version=5.25.2&version=5.25.3&version=5.25.4&version=5.26.0&version=git-master&version=git%20master&version=Master)! Берите любую и избавьте нас от неё!

## Новые возможности

На странице «Ночная цветовая схема» Параметров системы теперь можно установить дневную цветовую температуру в дополнение к ночной, для максимальной гибкости (Natalie Clarius, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=390021)):

![0](https://pointieststick.files.wordpress.com/2022/09/day-color.jpg)

Центр приложений Discover теперь отображает возрастные ограничения для приложений, которые их указывают! (Aleix Pol Gonzalez, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/357)):

![1](https://pointieststick.files.wordpress.com/2022/09/content-rating.jpg)

Discover теперь позволяет изменить имя, используемое для отправки отзывов о приложениях (Bernardo Gomes Negri, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=405935))

В Discover на странице каждого приложения теперь есть кнопка «Поделиться», которая позволяет отправить кому-либо ссылку на это приложение (Aleix Pol Gonzalez, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458464))

Discover теперь проверяет перед обновлением, достаточно ли у вас свободного места, и предупреждает, когда его нет (Aleix Pol Gonzalez, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457868))

Теперь вы можете настроить, что происходит, когда вы активируете окно, которое в данный момент находится на другом виртуальном рабочем столе: вы переключаетесь на виртуальный рабочий стол с этим окном (поведение по умолчанию) или же окно перемещается на текущий рабочий стол (Natalie Clarius, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=438375)):

![2](https://pointieststick.files.wordpress.com/2022/09/vd-window-options.jpg)

*Параметры системы > Диспетчер окон > Поведение окон > Дополнительно*

## Улучшения пользовательского интерфейса

При использовании нескольких мониторов положение окон теперь запоминается для каждого из них, поэтому при отключении и подключении мониторов окна, за это время не перемещённые вручную, будут автоматически возвращены на последний помнящий их экран (Xaver Hugl, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/2573))

Уведомления о сопряжении устройств Bluetooth, их запросы разрешений и т. д. теперь будут появляться даже в режиме «Не беспокоить», чтобы вы их не пропустили (Nicolas Fella, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/bluedevil/-/merge_requests/71))

Всплывающее окно виджета «Выбор цвета» теперь выводит сообщение-заполнитель, если в нём нет цветов, и позволяет удалить сохранённые цвета (Fushan Wen, Plasma 5.26. [Ссылка 1](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/227) и [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/225))

Компактное представление отдельного виджета «Проигрыватель» (не тот значок, который по умолчанию отображается в системном лотке) теперь показывает название, исполнителя и обложку альбома воспроизводимой в данный момент дорожки (Fushan Wen, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1892)):

![3](https://pointieststick.files.wordpress.com/2022/09/album-art.jpg)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Когда заряд батареи достигает «критически низкого» порога, яркость экрана больше не повышается, если она уже ниже выбранного для этой ситуации автоматически применяемого уровня (Louis Moureaux, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=394945))

Применение набора курсоров, наследующего самого себя, больше не делает невозможным вход в учётную запись пользователя (Влад Загородний, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457926))

В сеансе Plasma Wayland диспетчер окон KWin больше не падает иногда при перетаскивании вложения из почтовой программы Thunderbird (Влад Загородний, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458226))

В Discover нажатие кнопки для очистки пользовательских данных удалённого приложения, которое было установлено из локального пакета Flatpak (а не обычного файла `.flatpakref` или репозитория), больше не удаляет все пользовательские данные всех приложений Flatpak (Aleix Pol Gonzalez, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458490))

Другие сведения об ошибках, которые могут вас заинтересовать:

* 49 15-минутных ошибок Plasma (на прошлой неделе было 46). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 21 ошибка Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 112 ошибок исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-08-26&chfieldto=2022-09-02&chfieldvalue=RESOLVED&list_id=2144081&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/09/02/this-week-in-kde-day-color/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: system tray → системный лоток -->
