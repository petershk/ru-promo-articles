# На этой неделе в KDE: новый Spectacle

> 4–11 декабря, основное — прим. переводчика

На этой неделе [интерфейс утилиты для создания снимков экрана Spectacle был переписан на QML](https://invent.kde.org/graphics/spectacle/-/merge_requests/150), что упрощает дальнейшую разработку приложения и в будущем позволит нам добавить функцию записи экрана! Попутно мы встроили функцию комментирования непосредственно в режим выбора прямоугольной области! К слову, этот режим стал куда отзывчивее. Наконец, проделанная работа также позволила закрыть 12 отчётов об ошибках.

![0](https://pointieststick.files.wordpress.com/2022/12/annotations-in-rectangular-region-mode.jpg)

*Комментирование доступно прямо во время выбора области*

![1](https://pointieststick.files.wordpress.com/2022/12/very-meta-new-main-window.jpg)

*Новый интерфейс главного окна (он ещё будет улучшаться, ведь теперь его легко менять)*

Большое спасибо Noah Davis и Marco Martin, долгое время усердно работавшим над этим! Новый Spectacle выйдет под версией 23.04.

## Другие новые возможности

В диспетчере окон KWin появились действия для перемещения окна на соседние экраны (влево/вправо/вверх/вниз); на них можно назначить комбинации клавиш (Natalie Clarius, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=453038))

## Другие улучшения пользовательского интерфейса

Во всплывающей подсказке виджета Bluetooth теперь отображается состояние батареи любых подключённых устройств, умеющих его сообщать (Иван Ткаченко, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/bluedevil/-/merge_requests/105)):

![2](https://pointieststick.files.wordpress.com/2022/12/bluetooth-with-battery-percentage.png)

Во всплывающих подсказках виджетов «Батарея и яркость» и «Проигрыватель» теперь указано, что для взаимодействия с ними можно использовать прокрутку над их значками колёсиком мыши. Кажется, теперь всё! (Nate Graham и Nicolai Weitkemper, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/commit/537f50a397eee64718806686d5a5045bd1112eab) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2414))

Список виджетов в диалоге настройки системного лотка теперь полностью управляем с помощью клавиатуры (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2405))

Расчётное время до полной разрядки батареи теперь сглаживается, поэтому оно не будет резко уменьшаться или увеличиваться из-за кратковременных скачков энергопотребления (Fushan Wen, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=434432))

Прокрутка над значком виджета «Проигрыватель» для регулировки громкости приложения, воспроизводящего мультимедиа, теперь увеличивает или уменьшает громкость с шагом, настроенным в Параметрах системы, вместо того чтобы использовать для этого аналогичную настройку в самом виджете (Bharadwaj Raju, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2421))

Боковые шторки в приложениях на основе библиотеки Kirigami теперь можно закрывать с помощью клавиши Esc или щелчком по пустой затемнённой области (Matej Starc, Frameworks 5.102. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454119))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Когда установлена служба Plasma для интеграции с браузерами, виджет «Проигрыватель» больше не отображает два набора элементов управления воспроизведением (Bharadwaj Raju, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1088))

В сеансе Plasma Wayland исправлена пара специфичных проблем, связанных с вставкой: больше нет задержки при закрытии виджетов Plasma после копирования файлов в диспетчере файлов Dolphin и его закрытия; текст, скопированный из виджета буфера обмена, теперь можно вставлять в текстовые поля других виджетов Plasma (David Redondo, Frameworks 5.102. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=454379) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=442521))

При удалении какого-либо виджета и завершении процесса `plasmashell` (в том числе при перезагрузке системы), когда уведомление «Виджет удалён» ещё отображается на экране, виджет больше не возвращается со следующим запуском Plasma (Marco Martin, Frameworks 5.102. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=417114))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 6 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 7). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 46 (на прошлой неделе было 47). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 102 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-12-02&chfieldto=2022-12-9&chfieldvalue=RESOLVED&list_id=2226233&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/12/09/this-week-in-kde-new-spectacle/>  

<!-- 💡: Escape → Esc -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: bug tracker → система отслеживания ошибок -->
<!-- 💡: system tray → системный лоток -->
