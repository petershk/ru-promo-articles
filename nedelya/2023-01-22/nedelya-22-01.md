# На этой неделе в KDE: лучший выпуск Plasma 5

Ветка для выпуска Plasma 5.27 отделилась — ознакомьтесь с [нашим анонсом](https://kde.org/announcements/plasma/5/5.26.90/) и [протестируйте бета-версию](https://community.kde.org/Plasma/Live_Images#Ships_Plasma_5.27_Beta)! Вы, вероятно, заметили, что число 15-минутных ошибок и ошибок с очень высоким приоритетом в Plasma подросло, так что на предстоящих трёх неделях бета-тестирования мы сосредоточимся на борьбе с ними. Кроме того, Plasma 5.27 продолжит получать регулярные корректирующие выпуски вплоть до выхода Plasma 6, который, как мы очень надеемся, состоится до конца этого года! Захватывающие времена.

## Новые возможности

Авторы обоев теперь могут вручную задать цвет выделения, соответствующий их обоям, и он будет использоваться вместо автоматически определённого, если пользователь выбрал опцию «Цвет выделения: в соответствии с выбранными обоями» (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2084))

Режим «Не беспокоить» теперь можно включить и из командной строки, выполнив `kde-inhibit --notifications` (Jakub Nowak, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kde-cli-tools/-/merge_requests/45))

## Улучшения пользовательского интерфейса

Обложки альбомов высокого разрешения стали отображаться чётче в музыкальном проигрывателе Elisa при использовании масштабирования экрана (Nate Graham, Elisa 22.12.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464342))

Диспетчер окон KWin теперь по умолчанию старается обеспечить максимально плавную анимацию (баланс между плавными анимациями и низкой задержкой настраивается пользователем), что особенно заметно в сеансе Plasma Wayland на встроенных графических чипах Intel (Влад Загородний, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3420))

Виджет калькулятора теперь позволяет копировать результат вычисления, а также удалять цифры клавишей Backspace (Martin Frueh, Plasma 5.27. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=464155) и [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/321))

Виджет калькулятора больше не отображается среди результатов поиска в строке KRunner, меню запуска приложений и эффекте «Обзор»: он открывался в отдельном окне, из-за чего многие думали, что это стандартное приложение калькулятора, либо не понимали, почему в системе установлены два калькулятора (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/315))

Страницы Параметров системы «Комбинации клавиш» и «Права доступа пакетов Flatpak» теперь используют наш более современный безрамочный стиль (Nate Graham, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1341) и [ссылка 2](https://invent.kde.org/plasma/flatpak-kcm/-/merge_requests/18)):

![0](https://pointieststick.files.wordpress.com/2023/01/shortcuts-after.png)

![1](https://pointieststick.files.wordpress.com/2023/01/flatpak-after.png)

Виджет «Список окон» теперь отображает резервный значок для приложений, не предоставивших подходящего значка, а также логотип Plasma, когда активен рабочий стол (Guilherme Marçal Silva, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1336))

В диалоге настройки системного лотка к названиям некоторых элементов больше не добавляется примечание «(загружается автоматически)», так как это лишь деталь реализации, ни о чём не говорящая пользователям (Nicolas Fella, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2495))

В приложениях KDE на основе QtWidgets всплывающие подсказки больше не могут отображать один и тот же текст дважды: один раз в самой всплывающей подсказке, другой — для расширенного пояснения функции «Что это?» (Joshua Goins, Frameworks 5.103. [Ссылка](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/139))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Устранено аварийное завершение работы KWin, иногда возникавшее при закрытии одного из экземпляров приложения, запущенного несколько раз, когда включён эффект «Затемнение неактивных окон» (Влад Загородний, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=442222))

В KWin дополнительно реализована более старая версия протокола Wayland `text-input`, благодаря чему различные методы ввода будут работать в браузерах семейства Chromium и приложениях на основе Electron (Xuetian Weng, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3403))

В сеансе Wayland реализована базовая поддержка залипания клавиш; работа над ней продолжится (Nicolas Fella, Plasma 5.27 [Ссылка](https://bugs.kde.org/show_bug.cgi?id=444335))

Исправлены различные недавно появившиеся визуальные недочёты, затрагивавшие значки в панели задач и системном лотке, в том числе постоянно мерцающие значки некоторых приложений на основе Qt 6. Также исправлен автоматический тест, проверяющий это, что позволит избежать подобных недочётов в будущем (Arjen Hiemstra, Frameworks 5.103. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=463061) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=463685))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 10 ошибок в Plasma с очень высоким приоритетом (на 3 больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 53 (на 3 больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 118 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-01-13&chfieldto=2023-01-20&chfieldvalue=RESOLVED&list_id=2255330&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлен автоматический тест для навигации во всплывающем окне календаря из виджета цифровых часов (Marco Martin, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2491))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/01/20/this-week-in-kde-the-best-plasma-5-version-ever/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
